import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.core.Logger;
import org.dom4j.DocumentException;
import org.hibernate.Query;
import org.hibernate.Session;

import spider.dao.Content;
import spider.utils.hibernate.HibernateUtil;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-9-2
 * @ModifyTime:2014-9-2
 */
public class Test {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws DocumentException 
	 */
	public static void main(String[] args) throws IOException, DocumentException {
        //createAndStorePerson();  //创建保存数据
        //getData();  //获取数据库数据
		
		//get获取网页数据
		//HttpGetConnect connect = new HttpGetConnect();
		//connect.connect("http://bdtool.net/");
		
		//jsoup处理数据
		//HtmlManage manage = new HtmlManage();
		//Document doc = manage.manageDirect("http://bdtool.net/");
		
		//List<String> list = manage.manageHtmlClass(doc, "list-group-item");
		//for(int i = 0; i < list.size(); i++){
		//	log.info(list.get(i).toString());
		//}
		int i = 0;
		//while(Data.getInstance().isLaunch()){
		//	log.info(i ++ );
		//}
		
		//XmlManage xml = new XmlManage();
		//xml.xmlManage();
		
		
	}



   private static void createAndStorePerson() {

       Session session =                   // 通过Session工厂获取Session对象
    		   HibernateUtil.getSessionFactory().getCurrentSession();
       session.beginTransaction();         //开始事务
     
       Content content = new Content();
       content.setTitle("11111111111111");
       session.save(content);
       session.getTransaction().commit(); // 提交事务
   }
   
   private static void getData(){
	   Session session =                   // 通过Session工厂获取Session对象
    		   HibernateUtil.getSessionFactory().getCurrentSession();
	   session.beginTransaction();         //开始事务
	   //from后面是对象，不是表名
	   String hql="from Content as content where content.title=:name";//使用命名参数，推荐使用，易读。
	   Query query=session.createQuery(hql);
	   query.setString("name", "11111111111111");
	   
	   List<Content> list=query.list();
	   
	   for(Content content:list){
		   System.out.println(content.getTitle());
	   }
	   if(session!=null)
		   session.close();
   }
   
   private static Log log = LogFactory.getLog(Test.class);
}
