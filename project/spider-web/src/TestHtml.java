import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import spider.utils.html.HtmlContentParser;
import spider.utils.http.HttpGetConnect;
/**
 * 网页降噪处理流程事例
 * @author Administrator
 *
 */
public class TestHtml {

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        HttpGetConnect get = new HttpGetConnect();
        String url = "http://www.cnblogs.com/yjmyzz/p/spring-security-with-spring-mvc-helloworld.html";
        URI uri = new URI(url);
        String host = uri.getHost();
        String content = get.connect( url, "UTF-8");//"<html><head><title>DOM Tutorial</title></head><body> <h1>DOM Lesson one</h1> <p>Hello world!</p> </body></html>";//
        Document doc = Jsoup.parse(HtmlContentParser.clean(content));
        //System.out.println(doc.html());
        String title = doc.title().split("-")[0];
        //System.out.println(title);
        Element detail = doc.body();
        //System.out.println(detail);
        
        String ik = HtmlContentParser.analyzer(title);
        while(true){
            Elements elements1 = detail.children();
            //System.out.println(elements1.size());
            if(elements1.select("style").size() > 0 || elements1.select("script").size() > 0 || 
            		elements1.select("iframe").size() > 0){
                detail = elements1.first();
                Iterator<Element> it = elements1.listIterator();
                while(it.hasNext()){
                    //System.out.println("2");
                    Element item = it.next();
                    if(!item.tagName().equals("style") && !item.tagName().equals("script")){
                    	
                    	if(HtmlContentParser.analyzerNum(detail,ik) < HtmlContentParser.analyzerNum(item,ik)){
                    		detail = item;
                    	}
                    	/*int num = textNum(detail);
                    	if(textNum(detail) < textNum(item)){
	                        detail = item;
	                    }*/
                    }
                }
            }else{
                break;
            }
        }
        detail.tagName("p");
        System.out.println(detail.html());
        System.out.println(HtmlContentParser.textNum(detail));
    }

}
