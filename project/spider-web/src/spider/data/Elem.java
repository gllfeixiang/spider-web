package spider.data;

import java.util.List;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-29
 * @ModifyTime:2014-11-29
 */
public class Elem {
	private String name;
	private List<Attr> attrs;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Attr> getAttrs() {
		return attrs;
	}
	public void setAttrs(List<Attr> attrs) {
		this.attrs = attrs;
	}
}
