package spider.data;
/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-29
 * @ModifyTime:2014-11-29
 */
public class Tag {
	private String name; //名称
	private String pro; //属性
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPro() {
		return pro;
	}
	public void setPro(String pro) {
		this.pro = pro;
	}
}
