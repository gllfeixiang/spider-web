package spider.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-13
 * @ModifyTime:2014-11-13
 */
public class Data {
	
	public static boolean launch = false;   // true 为运行中
	public static Date startTime = null; //启动时间
	
	public static String url_type = "simple"; //简单的链接形式
	public static String url_head = ""; //爬行页面
	public static long url_start = 0; //爬行开始
	public static long url_end = 0; //爬行结束
	public static String url_suffix = ""; //爬行后缀
	
	public static Analysis analysis = new Analysis(); //解析数据
	
	public static boolean fixTime = false; //是否循环执行
	public static long timeInteval = 1000; //时间间隔 ms
	
	public static Timer timer = null;  //定时
	
}
