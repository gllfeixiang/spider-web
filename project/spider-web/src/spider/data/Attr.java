package spider.data;
/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-14
 * @ModifyTime:2014-11-14
 */
public class Attr {
	private String type;
	private int num = 0;
	private Tag tag;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public Tag getTag() {
		return tag;
	}
	public void setTag(Tag tag) {
		this.tag = tag;
	}
}
