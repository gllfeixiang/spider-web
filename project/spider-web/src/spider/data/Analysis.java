package spider.data;

import java.util.List;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-29
 * @ModifyTime:2014-11-29
 */
public class Analysis {
	private String type ;
	private List<Elem> elements;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Elem> getElements() {
		return elements;
	}
	public void setElements(List<Elem> elements) {
		this.elements = elements;
	}
}
