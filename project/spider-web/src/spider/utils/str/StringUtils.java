package spider.utils.str;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-29
 * @ModifyTime:2014-11-29
 */
public class StringUtils {
	private static Logger logger = LogManager.getLogger(StringUtils.class.getName());  
    
	public static void main(String[] args) throws MalformedURLException{
		log.info("aaaaaaaaaaaaaaaa");
		logger.error("bbbbbbbbbbbbbbb");
		logger.info(hostUrl("http://www.baidu.com/baidu?tn=monline_5_dg&ie=utf-8&wd=java+url%E8%8E%B7%E5%8F%96%E5%9F%9F%E5%90%8D"));
	}

	/**
	 * 根据正则，删除多余的部分
	 * @param str
	 * @param deleteRegex
	 * @return
	 */
	public static String cutString(String str,String deleteRegex){
		return str.replaceAll(deleteRegex, "");
	}
	
	/**
	 * 获取一个URL的域名部分
	 * @param uri
	 * @return
	 * @throws MalformedURLException
	 */
	public static String hostUrl(String uri) throws MalformedURLException{
		URL url = new URL(uri);
		return url.getHost();
	}
	
	private static Log log = LogFactory.getLog(StringUtils.class);
}
