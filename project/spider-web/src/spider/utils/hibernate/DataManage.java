package spider.utils.hibernate;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;

import spider.dao.Content;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-14
 * @ModifyTime:2014-11-14
 */
public class DataManage {
	private Session session = null;
	public DataManage(){
		session =                   // 通过Session工厂获取Session对象
	    	HibernateUtil.getSessionFactory().getCurrentSession();
	}
	public void Store(Object entity) {

       session.beginTransaction();         //开始事务
       session.save(entity);
       session.getTransaction().commit(); // 提交事务
   }
   
   public void getData(){
	   session.beginTransaction();         //开始事务
	   //from后面是对象，不是表名
	   String hql="from Content as content where content.title=:name";//使用命名参数，推荐使用，易读。
	   Query query=session.createQuery(hql);
	   query.setString("name", "11111111111111");
	   
	   List<Content> list=query.list();
	   
	   for(Content content:list){
		   System.out.println(content.getTitle());
	   }
	   if(session!= null)
		   session.close();
   }
   
   public void close(){
	   if(session!= null)
		   session.close();
   }
   
   private static Log log = LogFactory.getLog(DataManage.class);
}
