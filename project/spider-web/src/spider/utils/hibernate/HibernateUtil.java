package spider.utils.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-9-2
 * @ModifyTime:2014-9-2
 */
public class HibernateUtil {
    private static SessionFactory sessionFactory;
    private static void createSession(){
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    public static SessionFactory getSessionFactory() {
    	if(sessionFactory == null){
    		createSession();
    	}
        return sessionFactory;
    }

}