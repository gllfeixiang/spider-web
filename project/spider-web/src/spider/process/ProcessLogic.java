package spider.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;

import spider.data.Attr;
import spider.data.Data;
import spider.data.Elem;
import spider.data.Tag;
import spider.utils.html.HtmlManage;
import spider.utils.http.HttpGetConnect;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-28
 * @ModifyTime:2014-11-28
 */
public class ProcessLogic {
	
	public void processAnalysis(String url) throws IOException{
	    HtmlManage manage = new HtmlManage();
		HttpGetConnect get = new HttpGetConnect();
		List<Map<String,Object>> list = null;
		if(Data.analysis.getType().equals("single")){
			log.info("processSingle");
			list = processSingle(manage,get,url);
		}else{
			log.info("processList");
			list = processList(manage,get,url);
		}
		
		for(int i = 0; i < list.size(); i++){
			Map<String,Object> map = list.get(i);
			log.info("title is " + map.get("title").toString());
			log.info("content is " + map.get("content").toString());
		}
	}
	
	private List<Map<String,Object>> processSingle(HtmlManage manage,HttpGetConnect get,String url) throws IOException{
		List<Elem> elems = Data.analysis.getElements();
		
		List<Map<String,Object>> results = new ArrayList<Map<String,Object>>();
		Map<String,Object> result = new HashMap<String,Object>();
		for(int a = 0; a<elems.size();a++){
			Elem elem = (Elem)elems.get(a);
			String elemName = elem.getName();
			List<Attr> attrs = (List<Attr>)elem.getAttrs();
			Document docu = manage.manage(get.connect(url,"GB2312"));
			List<String> atem = null;

			for(int b = 0; b<attrs.size();b++){
				Attr at = attrs.get(b);
				Tag tag = at.getTag();
				//log.info(at.getName());
				if(at.getType().equals("tag")){
					atem = manage.manageHtmlTag(docu, tag.getName());
				}else if(at.getType().equals("class")){
					atem = manage.manageHtmlClass(docu, tag.getName());
				}else{
					atem = manage.manageHtmlKey(docu,tag.getName(),tag.getPro());
				}
				docu = manage.manage(atem.get(0));
			}
			result.put(elemName, atem.get(0));
		}
		results.add(result);
		return results;
	}
	
	private List<Map<String,Object>> processList(HtmlManage manage,HttpGetConnect get,String url) throws IOException{
		List<Elem> elems = Data.analysis.getElements();
		int count = 0;
		List<Map<String,Object>> results = new ArrayList<Map<String,Object>>();
		List<List<String>> resultTem = new ArrayList<List<String>>();
		List<String> elemNames = new ArrayList<String>();
		Document doc = manage.manage(get.connect(url,"UTF-8"));
		log.info(doc.html());
		for(int a = 0; a<elems.size();a++){
			Elem elem = (Elem)elems.get(a);
			String elemName = elem.getName();
			elemNames.add(elemName);
			List<Attr> attrs = (List<Attr>)elem.getAttrs();
			
			List<String> tem = new ArrayList<String>();
			//获取多少组信息
			Attr attem = attrs.get(0);
			Tag tag = attem.getTag();
			if(attem.getType().equals("tag")){
				tem = manage.manageHtmlTag(doc, tag.getName());
			}else if(attem.getType().equals("class")){
				tem = manage.manageHtmlClass(doc, tag.getName());
			}else{
				tem = manage.manageHtmlKey(doc,tag.getName(),tag.getPro());
			}
			count = tem.size();
			
			//获取每一组中每一个元素的列表
			List<String> elements = new ArrayList<String>();
			for(int i = 0 ; i < tem.size() ; i++){
				String html = tem.get(i);
				Document docu = manage.manage(html);

				List<String> atem = null;
				for(int b = 1; b<attrs.size();b++){
					Attr at = attrs.get(b);
					Tag ta = at.getTag();
					log.info(ta.getName());
					if(at.getType().equals("tag")){
						atem = manage.manageHtmlTag(docu, ta.getName());
					}else if(at.getType().equals("class")){
						atem = manage.manageHtmlClass(docu, ta.getName());
					}else{
						atem = manage.manageHtmlKey(docu,ta.getName(),ta.getPro());
					}
					docu = manage.manage(atem.get(0));
					
				}
				elements.add(atem.get(0));
			}
			
			resultTem.add(elements);
		}
		
		//处理
		for(int j = 0 ; j < count; j++){
			Map<String,Object> map = new HashMap<String,Object>();
			for(int m = 0; m < elemNames.size(); m++){
				map.put(elemNames.get(m), resultTem.get(m).get(j));
			}
			results.add(map);
		}
		
		return results;
	}
	
	private Log log = LogFactory.getLog(getClass());
}
