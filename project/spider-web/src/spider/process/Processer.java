package spider.process;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;

import spider.data.Attr;
import spider.data.Data;
import spider.utils.hibernate.DataManage;
import spider.utils.html.HtmlManage;
import spider.utils.http.HttpGetConnect;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-11-28
 * @ModifyTime:2014-11-28
 */
public class Processer extends TimerTask{

	@Override
	public void run() {
		log.info("执行");
		// TODO Auto-generated method stub
		DataManage dataManage = new DataManage();
	    
		try {
		    //单个页面爬行
		    if(Data.url_type.equals("simple")){
		    	ProcessLogic process = new ProcessLogic();
				
					process.processAnalysis(Data.url_head);
		    }else{  //批量页面爬行
		    	for(long n = Data.url_start; n < Data.url_end; n++){
		    		String url = Data.url_head + n + Data.url_suffix;
		    		
					ProcessLogic process = new ProcessLogic();
					process.processAnalysis(url);
					
		    		if(!Data.launch){
		    			break;
		    		}
		    	}
		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    	
    	dataManage.close();
	}
	
	private Log log = LogFactory.getLog(getClass());

}
