package spider.dao;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @说明：
 * @author: gaoll
 * @CreateTime:2014-9-2
 * @ModifyTime:2014-9-2
 */
@Entity
@Table(name="s_content")
public class Content extends DaomainId{
	
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
