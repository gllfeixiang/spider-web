package spider.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * @说明：
 * @author: gaoll  
 * @Email:1025581299@qq.com
 * @CreateTime:2013-12-29
 * @ModifyTime:2013-12-29
 */
@MappedSuperclass
public class DaomainId  implements Serializable  {
	private static final long serialVersionUID = 7325809706312057006L;
	@Id
	@GeneratedValue(generator="hibseq")
	@GenericGenerator(name="hibseq", strategy = "hilo",
	    parameters = {
	        @Parameter(name="max_lo", value = "1")
	    })
	private long id;
	
	
	@Column(nullable = false, insertable = false, updatable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
