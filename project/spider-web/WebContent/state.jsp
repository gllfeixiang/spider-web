<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="spider.data.Data"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>爬虫定制页面</title>
<script type="text/javascript" src="/js/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/js/state.js"></script>
</head>
<body>
	<!-- 当前时间 -->
	<% SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String createtime = sdf.format(new Date()); %>
	当前时间：<%= createtime %> <br/>
	
	<%String startTime = Data.startTime == null ? "未执行":sdf.format(Data.startTime);
	  String runTime = "";
	  if(Data.startTime != null){
		  Date now = new Date();
		  Date his = Data.startTime;
		  long l = now.getTime() - his.getTime();
		  long day=l/(24*60*60*1000);
		  long hour=(l/(60*60*1000)-day*24);
		  long min=((l/(60*1000))-day*24*60-hour*60);
		  long s=(l/1000-day*24*60*60-hour*60*60-min*60);
		  
		  runTime = day + " 天 " + hour + " 小时 " + min + " 分 " + s + " 秒";
	  }
	%>
	<% boolean run = Data.launch; %>
	启动时间：<%=startTime %><br/>
	执行时间：<%= runTime %><br/></br>
	<!-- 配置 -->
	==============================================配置==============================================
	<form action="RunSpider.do" method="get">
		<input type="hidden" name="state" value="1"></input> <!-- 1是启动发送的，0是终止发送的 -->
		
		
		<%--地址：<input type="text" name="url_head" style="width:300px;" value="<%=Data.url_head %>"></input>
		&nbsp;起：<input type="text" name="url_start" style="width:80px;" value="<%=Data.url_start %>">
		-&nbsp;终：<input type="text" name="url_end" style="width:80px;" value="<%=Data.url_end %>"></input>
		&nbsp;后缀：<input type="text" name="url_suffix" style="width:80px;" value="<%=Data.url_suffix %>"></input>
		<br/>
		属性：<input type="text" name="attr" style="width:150px;" value="<%=Data.attr %>"></input><br/>
		属性值：<input type="text" name="attrContent" style="width:200px;" value="<%=Data.attrContent %>"></input><br/>
		 --%>
		 
		循环执行：<input type="checkbox" name="fixTime" <%if(Data.fixTime){ %>checked="checked;"<%} %>></input><br/>
		循环间隔时间：<input type="text" name="timeInteval" style="width:100px;" value="<%=Data.timeInteval %>"></input> 毫秒
		
		<br/>
		<input type="submit" <%if(run){ %>disabled="disabled;"<%} %> value="执行" />
	</form>
	
	<!-- 终止 -->
	<form action="RunSpider.do",method="get">
		<input type="hidden" name="state" value="0"></input>
		<input type="submit" <%if(!run){ %>disabled="disabled;"<%} %> value="终止"> </input>
	</form>
	
</body>
</html>